Drush Webform Health
====================

Check the status of webforms using drush. The module currently supports the
following commands:

`drush webform-email-check`
Check that all of the webforms on a website have email addresses associated with
them.
